<?php get_header(); the_post(); ?>
    <section class="page-static-template comp static-hero hero" data-tracking-container="true">
        <div class="hero-container">
            <div class="g g-two-up">
                <div class="g-main">
                    <h1 class="hero-title">
                        <?php the_title(); ?>
                    </h1>
                </div>
            </div>
        </div>
    </section>
    <article class="comp right-rail static-page-content article">
        <div class="comp structured-content article-content expert-content right-rail__offset lock-journey mntl-sc-page mntl-block">
            <?php the_content(); ?>
        </div>
    </article>
<?php get_footer(); ?>