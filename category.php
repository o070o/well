<?php
    global $wp_query;
    $category = get_queried_object();
    get_header();
    $level1 = $level2 = $level3 = NULL;
?>
    <?php if($category->parent == 0): ?>
	<section class="comp hero image-bleed">
		<div class="hero-container">
			<div class="g g-two-up">
				<div class="g-main">
					<h1 class="hero-title">
						<?php single_cat_title(); ?>
					</h1>
				</div>
				<div class="loc secondary g-secondary">
					<div id="hero-image_1-0" class="comp hero-image mntl-block">
						<img src="<?php the_field('background_image', get_queried_object()) ?>" />
					</div>
				</div>
			</div>
		</div>
	</section>
    <?php
        else:
            //current level 3 category
            if(is_level3_category($category->term_id, 3) != NULL){
                $parent_id = is_level3_category($category->term_id, 3);
                $lvl2_cat = get_category($parent_id);
                $lvl1_cat = get_category(is_level3_category($category->term_id, 2));
                $level3 = true;
            //current level 2 category
            } else if(is_level3_category($category->term_id, 2) != NULL) {
                $parent_id = is_level3_category($category->term_id, 2);
                $lvl2_cat = get_category($parent_id);
                $lvl1_cat = get_category(is_level3_category($category->term_id, 1));
                $level2 = true;
            //current level 1 category
            } else if(is_level3_category($category->term_id, 1) != NULL) {
                $parent_id = is_level3_category($category->term_id, 1);
                $lvl2_cat = get_category($parent_id);
                $lvl1_cat = $category;
                $level1 = true;
            }
    ?>
    <div class="comp is-horizontal has-image hero-complex" style="">
        <div class="hero-complex-body">
            <div class="hero-complex-kicker">
                <a href="<?php echo get_category_link($lvl2_cat); ?>">
                    <?php echo $lvl2_cat->name; ?>
                </a>
            </div>
            <h1 class="hero-complex-title"><?php the_field('hero_title', get_queried_object()) ?></h1>
            <div class="hero-complex-desc">
				<?php echo category_description(); ?>
			</div>
        </div>
        <div class="hero-complex-img">
            <img src="<?php the_field('background_image', get_queried_object()) ?>">
        </div>
    </div>
    <?php endif; ?>
	<div class="l-left-sidebar ">
		<div class="loc sidebar l-sidebar">
			<div id="breadcrumbs-list_1-0" class="comp is-full is-expandable breadcrumbs-list">
                <?php
                    if($category->parent == 0){
                        $parent_id = get_queried_object_id();
                        $childs = get_categories([
                            'parent' => $parent_id,
                            'hide_empty' => 0
                        ]);
                    } else {
                        if ($level3) {
                            $parent_id = is_level3_category($category->term_id, 2);
                        } else {
                            $parent_id = get_queried_object_id();
                        }
                        $childs = get_categories([
                            'parent' => $parent_id,
                            'hide_empty' => 0
                        ]);
                ?>
                <div class="breadcrumbs-list-header">
                    <?php if(!empty($childs)){ ?>
                    <span class="breadcrumbs-list-title">
                        <a href="<?php echo get_category_link($lvl2_cat); ?>">
                            <svg class="icon icon-arrow-left ">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-arrow-left"></use>
                            </svg>
                            <?php echo $lvl2_cat->name; ?>
                        </a>
                    </span>
                    <?php } ?>
                    <span class="breadcrumbs-list-subtitle">
                        <?php if(empty($childs)) { ?>
                        <a href="<?php echo get_category_link($lvl2_cat); ?>"> <?php echo $lvl2_cat->name; ?>
                        <?php } else { ?>
                        <a href="<?php echo get_category_link($lvl1_cat); ?>"> <?php echo $lvl1_cat->name; ?>
                        <?php } ?>
                            <svg class="breadcrumbs-list-icon icon-empty-caret">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-empty-caret"></use>
                            </svg>
                        </a>
                    </span>
                </div>
                <?php } ?>
				<ul class="breadcrumbs-list-list">
                    <?php
                        if($level2 == true && empty($childs)){
                            $childs = get_categories([
                                'parent' => is_level3_category($category->term_id, 2),
                                'hide_empty' => 0
                            ]);
                        }
                        if(!empty($childs)):
                            foreach ($childs as $child):
                    ?>
					<li class="breadcrumbs-list-item <?php echo $child->term_id == get_queried_object_id() ? 'is-active' : ''; ?>">
						<a href="<?php echo get_category_link($child); ?>"class="breadcrumbs-list-link <?php echo $child->term_id == get_queried_object_id() ? 'is-active' : ''; ?>">
                            <?php if($child->term_id == get_queried_object_id()): ?>
                            <svg class="breadcrumbs-list-icon-active icon-caret-right"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-caret-right"></use></svg>
                            <?php endif;  ?>
							<?php echo $child->name; ?>
							<svg class="breadcrumbs-list-icon icon-empty-caret">
								<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-empty-caret"></use>
							</svg>
						</a>
                        <?php if($child->term_id == get_queried_object_id()): ?>
                        <ul class="breadcrumbs-list-submenu"></ul>
                        <?php endif;  ?>
					</li>
                    <?php endforeach; endif;  ?>
				</ul>
				<a href="#fitness-4156989" class="text-btn">
					View More
					<svg class="text-btn-icon icon-circle-arrow-down">
						<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-circle-arrow-down"></use>
					</svg>
				</a>
			</div>
		</div>
		<div class="loc content l-main">
			    <?php get_template_part('template/sticky-posts/sticky', 'posts'); ?>
                <section id="article-list_1-0" class="comp article-list">
				<div class="loc content section-body">
					<ul id="posts_container" class="comp g g-three-up block-list" data-chunk="24">
                        <?php
                            query_posts(['cat'=>$category->term_id, 'tag__not_in'=>[59, 1532]]);
                            while (have_posts()):
                                the_post();
                                get_template_part('template/loop/content');
                            endwhile;
                        ?>
					</ul>
                    <?php if($wp_query->max_num_pages > 1): ?>
					<a href="#" data-cat="<?php echo get_queried_object_id(); ?>" data-current-page="1" data-total-pages="<?php echo  $wp_query->max_num_pages; ?>" class="btn-link n2t-load-more" aria-label="Xem thêm">
						<button class="btn btn-divider btn-dark" id="divider-button">
							<div class="btn-divider-inner">
								<span>Xem thêm</span>
								<svg class="icon icon-circle-arrow-down btn-icon">
									<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-circle-arrow-down"></use>
								</svg>
							</div>
						</button>
					</a>
                    <?php endif; ?>
				</div>
			</section>
			<?php
    		/*
			<div id="native-channel_1-0" class="comp native-channel native-homepage mntl-gpt-adunit gpt native " style="">
				<div id="native-channel" class="wrapper">
					<div style="border: 0pt none; width: 1px; height: 3px;"></div>
				</div>
			</div>
			*/
			?>
		</div>
	</div>
	<?php
	/*
	<div id="leaderboard-footer_1-0" class="comp has-right-label has-left-label leaderboard-footer leaderboard mntl-flexible-leaderboard mntl-flexible-ad mntl-gpt-adunit gpt leaderboard " style="">
		<div id="leaderboard2" class="wrapper">
			<div style="border: 0pt none; width: 728px; height: 91px;"></div>
		</div>
	</div>
	*/
	?>
<?php get_footer(); ?>