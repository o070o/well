<?php 
	get_header(); 
	global $wp_query;
?>

    <div id="search-layout_1-0" class="comp search-layout">
	<section class="loc content search-content">
		<div id="afs1" class="adsense afs" style="height: auto;"></div>
		<div id="search-results_2-0" class="comp search-results" data-tracking-container="true">
			<div class="search-content-header">
				<span class="num-results">Kết quả tìm kiếm </span>
				<div id="general-search_5-0" class="comp general-search" data-tracking-container="true">
					<form class="general-search-form" role="search" action="<?php echo home_url('/') ?>" method="get" data-suggestion="verywell">
						<div class="input-group">
							<button class="btn btn-submit is-hidden">
								<span class="is-vishidden">Tìm kiếm nội dung</span>
								<svg class="icon icon-magnifying-glass">
									<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-magnifying-glass"></use>
								</svg>
							</button>
							<button class="btn btn-clear">
								<span class="is-vishidden">Làm sạch</span>
								<svg class="icon icon-x">
									<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-x"></use>
								</svg>
							</button>
							<input type="text" name="s" id="search-input" class="general-search-input" placeholder="Search" aria-label="Search the site" required="required" value="<?php echo get_search_query(); ?>" autocomplete="off">
							<button class="btn btn-bright btn-go">Tìm kiếm</button>
						</div>
					</form>
				</div>
			</div>
			<ul>
                <?php
                    if(have_posts()):
                        while(have_posts()):
                            the_post();
                            $categories = wp_get_post_categories(get_the_ID());
                ?>
				<li class="loc item search-result-list-item">
					<a class="comp block-horizontal block" href="<?php the_permalink(); ?>" target="_blank">
						<div class="block__media">
							<div class="img-placeholder" style="padding-bottom:66.6%;">
								<?php echo get_the_post_thumbnail(get_the_ID(), 'large', ['class'=>'block__img']); ?>
							</div>
						</div>
						<div class="block__content" data-kicker="<?php echo isset($categories[0]) ? get_category($categories[0])->name : '';  ?>">
							<div class="block__title">
								<span><?php the_title() ?></span>
							</div>
							<p class="block__excerpt"> <?php the_excerpt(); ?></p>
							<span class="comp text-button text-btn" href="#">
								Đọc
								<svg class="text-btn-icon icon-circle-arrow-right">
									<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-circle-arrow-right"></use>
								</svg>
							</span>
						</div>
					</a>
				</li>
                <?php
                        endwhile;
                    endif;
                ?>
			</ul>
		</div>
		<?php 
			$big = 999999999; 
			echo paginate_links(array(
				'type' => 'list',
			    'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
			    'format' => '/page/%#%',
			    'current' => max( 1, get_query_var('paged') ),
			    'total' => $wp_query->max_num_pages,
			    'prev_text'    => __('PREV'),
			    'next_text'    => __('NEXT'),
			));
		?>
	</section>
</div>

<?php get_footer(); ?>
