(function ($) {

    $(document).on('click touchend', '.thumbs-up-button', function(event){
        event.preventDefault();
        $('.js-rating-section').hide();
        $('.js-success-section').removeClass('is-hidden');
    });

    $(document).on('click touchend', '.thumbs-down-button', function(event){
        event.preventDefault();
        $('.js-rating-section').hide();
        $('.js-feedback-section').removeClass('is-hidden');
    });

    $(document).on('click touchend', '.menu-button', function(event){
        event.preventDefault();

        $("html").toggleClass("is-noscroll");
        $("body").toggleClass("is-noscroll");
        $(".header-nav-panel").scrollTop(0);
        $(".header").toggleClass("is-fullnav");
        $(".main").toggleClass("is-invisible");
        $(".footer").toggleClass("is-invisible");

    });

    $(document).on('click touchend', '.search-button', function(event){
        if($(".jetpack-instant-search__overlay").length == 0){
            event.preventDefault();

            $(".header-search-input").focus();

            $("html").toggleClass("is-noscroll");
            $("body").toggleClass("is-noscroll");
            $(".header-nav-panel").scrollTop(0);
            $(".header").toggleClass("is-fullnav");
            $(".main").toggleClass("is-invisible");
            $(".footer").toggleClass("is-invisible");
        }
    });
    
    $(document).on('click touchend', '.mntl-toc__list li a', function(event){
        event.preventDefault();
        $('.mntl-toc__list li a').removeClass('active');
        $(this).addClass('active');
        $('html, body').animate({
            scrollTop: $($(this).attr('href')).offset().top
        }, 1000);
    });

    $(document).on('click touchend', '.mntl-expandable-block .icon', function(event){
        event.preventDefault();
        $('.mntl-expandable-block').toggleClass('is-expanded');
    });

    $(document).ready(function () {

        var ele = document.getElementById('tooltip');
        var sel = window.getSelection();
        var rel1= document.createRange();
        rel1.selectNode(document.getElementById('cal1'));
        var rel2= document.createRange();
        rel2.selectNode(document.getElementById('cal2'));

        window.addEventListener('mouseup', function () {
            if (!sel.isCollapsed) {
                var r = sel.getRangeAt(0).getBoundingClientRect();
                var rb1 = rel1.getBoundingClientRect();
                var rb2 = rel2.getBoundingClientRect();
                ele.style.top = (r.bottom - rb2.top)*100/(rb1.top-rb2.top) + 'px'; //this will place ele below the selection
                ele.style.left = (r.left - rb2.left)*100/(rb1.left-rb2.left) + 'px'; //this will align the right edges together

                //code to set content

                ele.style.display = 'block';
                $("#contact-form-comment-g2381-ontextmbnmunboli").val(sel.toString());
                setTimeout(function() {
                    $("#contact-form-comment-g2381-ontextsaukhichnhsa").focus();
                }, 0);
            }
        });

        // window.addEventListener('mousedown', function () {
        //     ele.style.display = 'none';
        // });

        $(document).on('click touchend', '.close-report-form', function(event){
            event.preventDefault();
            ele.style.display = 'none';
        });

        setTimeout(function () {
            $("li.view-all a").append('<svg class="text-btn-icon icon-circle-arrow-right"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-circle-arrow-right"></use></svg>');
        }, 1000);

        $(window).scroll(function(){
            if($(this).scrollTop() > 0){
                $(".header").addClass("header-condensed");
            } else {
                $(".header").removeClass("header-condensed");
            }
            
            if($('.toc').length > 0){
                if($(this).scrollTop() > 260){
                    $('.toc').addClass('is-fixed');
                } else {
                    $('.toc').removeClass('is-fixed');                
                }
            }
        });

        /*if($('#search_input').length > 0) {
            $('#search_input').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: 'POST',
                        dataType: 'json',
                        url: n2t.ajaxURL,
                        data: {
                            'action': 'n2t_search_posts',
                            'query': request.term
                        },
                        success: function (data) {
                            response($.map(data, function (v, i) {
                                return {
                                    label: v.post_title,
                                    value: v.guid
                                };
                            }));
                        }
                    });
                },
                select: function (event, ui) {
                    window.location.href = ui.item.value;
                }
            });
        }*/

        if($("a.n2t-load-more").length > 0) {
            $("a.n2t-load-more").on('click touchend', function (e) {
                e.preventDefault();
                var button = $(this),
                    current_page = button.attr('data-current-page'),
                    current_author = button.attr('data-author'),
                    current_cat = button.attr('data-cat'),
                    total_pages = button.attr('data-total-pages'),
                    container = $("#posts_container");
                if (!button.hasClass('loading')) {
                    $.ajax({
                        type: 'POST',
                        dataType: 'json',
                        url: n2t.ajaxURL,
                        data: {
                            'action': 'n2t_get_posts',
                            'current_page': current_page,
                            'cat': current_cat,
                            'author': current_author
                        },
                        beforeSend: function(){
                            button.addClass('loading').css('opacity', 0.5);
                        },
                        success: function (response) {
                            if (response.success == true) {
                                button.removeClass('loading').css('opacity', 1);
                                container.append(response.html);
                            }
                            button.attr('data-current-page', response.current_page);
                            if (response.current_page >= total_pages) {
                                button.hide();
                            }
                        }
                    });
                }
            });
        }
    });
}(jQuery));