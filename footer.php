			</main>
		<footer id="footer_1-0" class="comp footer" role="contentinfo" data-tracking-container="true">
			<div class="footer-inner">
				<div class="footer-social">
					<div id="newsletter-signup-vue_1-0" class="comp newsletter-signup-vue nl-loaded">
						<?php echo do_shortcode('[mc4wp_form id="44"]'); ?>
					</div>
					<div id="social-follow_1-0" class="comp extended social-follow static-social-follow mntl-block" data-tracking-container="true">
						<div id="social-follow__title_1-0" class="comp social-follow__title mntl-text-block">
							Follow Us
						</div>
						<div id="social-follow__intro_1-0" class="comp social-follow__intro mntl-text-block"></div>
						<ul id="social-follow__list_1-0" class="comp social-follow__list mntl-block">
							<li id="social-follow__item_1-0" class="comp social-follow__item social-follow__btn mntl-block">
								<a href="//www.facebook.com/well.songkhoe" target="_blank" rel="noopener" id="social-follow__link_1-0" class=" social-follow__link mntl-text-link social-follow__link--facebook" data-tracking-container="true">
									<span id="mntl-text-block_1-0" class="comp is-vishidden mntl-text-block">
									facebook</span>
									<svg class="icon icon-facebook ">
										<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-facebook"></use>
									</svg>
								</a>
							</li>
							<li id="social-follow__item_1-0-1" class="comp social-follow__item social-follow__btn mntl-block">
								<a href="//www.pinterest.com/well.songkhoe" target="_blank" rel="noopener" id="social-follow__link_1-0-1" class=" social-follow__link mntl-text-link social-follow__link--pinterest" data-tracking-container="true">
									<span id="mntl-text-block_1-0-1" class="comp is-vishidden mntl-text-block">
									pinterest</span>
									<svg class="icon icon-pinterest ">
										<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-pinterest"></use>
									</svg>
								</a>
							</li>
							<li id="social-follow__item_1-0-2" class="comp social-follow__item social-follow__btn mntl-block">
								<a href="//instagram.com/well.songkhoe" target="_blank" rel="noopener" id="social-follow__link_1-0-2" class=" social-follow__link mntl-text-link social-follow__link--instagram" data-tracking-container="true">
									<span id="mntl-text-block_1-0-2" class="comp is-vishidden mntl-text-block">
									instagram</span>
									<svg class="icon icon-instagram ">
										<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-instagram"></use>
									</svg>
								</a>
							</li>
							<li id="social-follow__item_1-0-3" class="comp social-follow__item social-follow__btn mntl-block">
								<a href="//flipboard.com/@well.songkhoe" target="_blank" rel="noopener" id="social-follow__link_1-0-3" class=" social-follow__link mntl-text-link social-follow__link--flipboard" data-tracking-container="true">
									<span id="mntl-text-block_1-0-3" class="comp is-vishidden mntl-text-block">
									flipboard</span>
									<svg class="icon icon-flipboard ">
										<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-flipboard"></use>
									</svg>
								</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="loc footer-nav">
					<nav id="tax1-nav_1-0" class="comp tax1-nav mntl-block" role="navigation">
						<ul id="link-list_1-0" class="comp link-list mntl-block">
                            <?php
                                wp_nav_menu( array(
                                    'items_wrap' => '%3$s',
                                    'theme_location' => 'footer1',
                                    'container'=>'',
                                    'link_class' => 'link-list-link'
                                ) );
                            ?>
						</ul>
					</nav>
					<ul id="footer-links_1-0" class="comp footer-links">
                        <?php
                            wp_nav_menu( array(
                                'items_wrap' => '%3$s',
                                'theme_location' => 'footer2',
                                'container'=>''
                            ) );
                        ?>
					</ul>
				</div>
			</div>
			<div class="loc footer-bottom">
				<div id="copyright_2-0" class="comp copyright mntl-text-block">
					© 2020 Well.vn - Lan tỏa giá trị sống xanh & tích cực
				</div>
			</div>

		</footer>
		<svg class="is-hidden">
			<defs>
				<symbol id="mntl-sc-block-starrating-icon">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
						<path d="M0,0v20h20V0H0z M14.2,12.2l1.1,6.3l-5.4-3.2l-5.1,3.2l1-6.3L1.4,8l5.9-0.8l2.6-5.8l2.7,5.8L18.5,8L14.2,12.2z"></path>
					</svg>
				</symbol>
			</defs>
		</svg>
        <?php wp_footer(); ?>
	</body>
</html>