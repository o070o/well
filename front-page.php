<?php get_header(); ?>
    <section id="hero_1-0" class="comp hero hero-grid-nav">
        <div class="hero-container">
            <div class="g g-two-up">
                <div class="g-main">
                    <h1 class="hero-title">
                        <?php echo get_bloginfo ( 'description' ); ?>
                    </h1>
                    <div id="general-search_2-0" class="comp general-search">
                        <form class="general-search-form" role="search" action="<?php echo home_url('/')  ?>" method="get" data-suggestion="verywell">
                            <div class="input-group">
                                <button class="btn btn-submit">
                                    <span class="is-vishidden">Search</span>
                                    <svg class="icon icon-magnifying-glass">
                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-magnifying-glass"></use>
                                    </svg>
                                </button>
                                <button class="btn btn-clear is-hidden">
                                    <span class="is-vishidden">Làm sạch</span>
                                    <svg class="icon icon-x">
                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-x"></use>
                                    </svg>
                                </button>
                                <input type="text" name="s" id="search_input" class="general-search-input" placeholder="Bạn đang tìm kiếm điều gì?" aria-label="Tìm nội dung trong website" required="required" value="">
                                <button class="btn btn-bright btn-go">Tìm kiếm</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="loc secondary g-secondary">
                    <section id="tags-section_2-0" class="comp tags-section">
                        <h2 class="tags-section-title">Chủ đề được quan tâm</h2>
                        <ul id="tags-nav_2-0" class="comp tags-nav link-list mntl-block">
                            <?php
                                $bookmarks = get_bookmarks( array(
                                    'orderby'        => 'name',
                                    'order'          => 'ASC',
                                    'category_name'  => 'Trending Topics'
                                ) );
                                if(!empty($bookmarks)){
                                foreach ( $bookmarks as $bookmark ) {
                            ?>
                            <li id="link-list-items_<?php echo $bookmark->link_id; ?>" class="comp tags-nav-item link-list-items link-list-item" data-ordinal="1">
                                <a href="<?php echo $bookmark->link_url; ?>" class="link-list-link tags-nav-link"> <?php echo $bookmark->link_name; ?></a>
                            </li>
                            <?php }} ?>
                        </ul>
                    </section>
                </div>
            </div>
        </div>
    </section>
    <?php the_post(); the_content(); ?>
<?php get_footer(); ?>