<?php
add_filter( 'body_class', 'body_class_before_header' );
function body_class_before_header( $classes ) {

    if ( is_page_template( 'page-toc.php' ) ) {
        $classes[] = 'corporateTemplate';
    }
    if ( is_page_template( 'page-about-us.php' ) ) {
        $classes[] = 'corporateTemplate aboutUsTemplate';
    }
    return $classes;
}

function n2t_callout_shortcode( $atts, $content = null ) {
	$a = shortcode_atts( array(
		'border_color' => 'red'
	), $atts );
	ob_start();
	?>
    <div class="comp theme-takeaway mntl-sc-block mntl-sc-block-callout mntl-block <?php echo $a['border_color']; ?>">
        <div class="comp expert-content mntl-sc-block-callout-body mntl-text-block">
            <?php echo $content; ?>
        </div>
    </div>
    <?php
	return ob_get_clean();
}
add_shortcode('callout', 'n2t_callout_shortcode');
function add_menu_link_class( $atts, $item, $args ) {
  if (property_exists($args, 'link_class')) {
    $atts['class'] = $args->link_class;
  }
  return $atts;
}
add_filter( 'nav_menu_link_attributes', 'add_menu_link_class', 1, 3 );

class Primary_Walker_Nav_Menu extends Walker_Nav_Menu {
    function start_lvl(&$output, $depth) {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<ul class=\"nav-sub\">\n";
    }
}

class FullScreen_Walker_Nav_Menu extends Walker_Nav_Menu {
    function start_lvl(&$output, $depth) {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<ul class=\"fullscreen-nav-sub\">\n";
    }
}

add_action('wp_ajax_nopriv_n2t_get_posts', 'n2t_get_posts');
add_action('wp_ajax_n2t_get_posts', 'n2t_get_posts');
function n2t_get_posts(){
    $html = '';
    $paged = $_POST['current_page'] + 1;
    $cat = $_POST['current_cat'];
    $query = new WP_Query([
        'cat' => $cat,
        'paged' => $paged,
        'tag__not_in' => [1532]
    ]);
    if($query->have_posts()):
        ob_start();
        while($query->have_posts()):
            $query->the_post();
            get_template_part('template/loop/content');
        endwhile;
        $html = ob_get_clean();
        wp_send_json(['success'=>true, 'html'=>$html, 'current_page'=>$paged]);
    else:
        wp_send_json(['success'=>false, 'html'=>$html, 'current_page'=>$paged]);
    endif;
}

add_action('wp_ajax_nopriv_n2t_search_posts', 'n2t_search_posts');
add_action('wp_ajax_n2t_search_posts', 'n2t_search_posts');
function n2t_search_posts(){
    $s = $_REQUEST['query'];
    $query = new WP_Query([
        'post_type' => 'post',
        's' => $s,
        'posts_per_page' => 6
    ]);
    wp_send_json($query->posts);
}

add_action('admin_init', function () {
    // Redirect any user trying to access comments page
    global $pagenow;
    
    if ($pagenow === 'edit-comments.php') {
        wp_redirect(admin_url());
        exit;
    }

    // Remove comments metabox from dashboard
    remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');

    // Disable support for comments and trackbacks in post types
    foreach (get_post_types() as $post_type) {
        if (post_type_supports($post_type, 'comments')) {
            remove_post_type_support($post_type, 'comments');
            remove_post_type_support($post_type, 'trackbacks');
        }
    }
});

// Close comments on the front-end
add_filter('comments_open', '__return_false', 20, 2);
add_filter('pings_open', '__return_false', 20, 2);

// Hide existing comments
add_filter('comments_array', '__return_empty_array', 10, 2);

// Remove comments page in menu
add_action('admin_menu', function () {
    remove_menu_page('edit-comments.php');
});

// Remove comments links from admin bar
add_action('init', function () {
    if (is_admin_bar_showing()) {
        remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
    }
});

function n2t_get_top_category($post_id = null, $level = 3){

    if($post_id == null){
        $post_id = get_the_ID();
    }

    $categories = wp_get_post_categories($post_id);

    if(!empty($categories) && isset($categories[0])){
        $i = 0;
        $catid = $categories[0];
        while ($catid) {
            $cat = get_category($catid); // get the object for the catid
            $catid = $cat->category_parent; // assign parent ID (if exists) to $catid
              // the while loop will continue whilst there is a $catid
              // when there is no longer a parent $catid will be NULL so we can assign our $catParent
            $catParent = $cat->cat_ID;
            $i++;
            if($i == $level){
                return $catParent;
            }
        }
    } else {
        return false;
    }
}

function is_level3_category($term_id, $level=3){
    $i=0;
    while ($term_id) {
        $cat = get_category($term_id); // get the object for the catid
        $term_id = $cat->category_parent; // assign parent ID (if exists) to $catid
          // the while loop will continue whilst there is a $catid
          // when there is no longer a parent $catid will be NULL so we can assign our $catParent
        $catParent = $cat->cat_ID;
        $i++;
        if($i == $level){
            return $catParent;
        }
    }
}

function n2t_theme_init() {
    $labels = array(
        'name'                  => _x( 'Trending Topics', 'Post type general name', 'recipe' ),
        'singular_name'         => _x( 'Trending Topic', 'Post type singular name', 'recipe' ),
        'menu_name'             => _x( 'Trending Topics', 'Admin Menu text', 'recipe' ),
        'name_admin_bar'        => _x( 'Trending Topic', 'Add New on Toolbar', 'recipe' ),
        'add_new'               => __( 'Add New', 'trending topic' ),
        'add_new_item'          => __( 'Add New trending topic', 'trending topic' ),
        'new_item'              => __( 'New trending topic', 'trending topic' ),
        'edit_item'             => __( 'Edit trending topic', 'trending topic' ),
        'view_item'             => __( 'View trending topic', 'trending topic' ),
        'all_items'             => __( 'All trending topics', 'trending topic' ),
        'search_items'          => __( 'Search trending topics', 'trending topic' ),
        'parent_item_colon'     => __( 'Parent trending topics:', 'trending topic' ),
        'not_found'             => __( 'No trending topics found.', 'trending topic' ),
        'not_found_in_trash'    => __( 'No trending topics found in Trash.', 'trending topic' ),
        'featured_image'        => _x( 'Trending Topic Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'trending topic' ),
        'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'trending topic' ),
        'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'trending topic' ),
        'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'trending topic' ),
        'archives'              => _x( 'Trending Topic archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'trending topic' ),
        'insert_into_item'      => _x( 'Insert into trending topic', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'trending topic' ),
        'uploaded_to_this_item' => _x( 'Uploaded to this trending topic', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'trending topic' ),
        'filter_items_list'     => _x( 'Filter trending topics list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'trending topic' ),
        'items_list_navigation' => _x( 'Trending Topics list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'trending topic' ),
        'items_list'            => _x( 'Trending Topics list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'trending topic' ),
    );
    $args = array(
        'labels'             => $labels,
        'description'        => 'Trending Topic custom post type.',
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'trending-topic' ),
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_position'      => 20,
        'supports'           => array( 'title' ),
        'show_in_rest'       => true
    );

    register_post_type( 'trending_topic', $args );
}
//add_action( 'init', 'n2t_theme_init' );

function atg_menu_classes($classes, $item, $args) {

    if ((in_array('current-post-ancestor', $classes) || in_array('current-menu-item', $classes) && !in_array('view-all', $classes)) ){
        $classes[] = 'active ';
    }

    if($args->theme_location == 'primary') {
        $classes[] = 'nav-list-item';
    }

    if($args->theme_location == 'footer2') {
        $classes[] = 'footer-links-item';
    }

    if($args->theme_location == 'footer1') {
        $classes[] = 'comp link-list-items link-list-item';
    }

    if($args->theme_location == 'tools') {
        $classes[] = 'breadcrumbs-list-item';
    }

    if($args->theme_location == 'header_fullscreen') {
        $classes[] = 'fullscreen-nav-list-item';
    }

    if($args->theme_location == 'header') {
        $classes[] = 'comp trusted-links-link link-list-items link-list-item';
    }

    return $classes;
}
add_filter('nav_menu_css_class', 'atg_menu_classes', 1, 3);

add_action('after_setup_theme', 'n2t_theme_setup');
function n2t_theme_setup(){
    add_theme_support( 'title-tag' );
    add_theme_support( 'post-thumbnails' );

    register_nav_menus( array(
        'header' => __( 'Header Bottom Menu' ),
        'tools' => __( 'Tools Menu' ),
        'header_fullscreen' => __( 'Header Fullscreen Menu' ),
        'primary'  => __( 'Primary Menu' ),
        'footer1'  => __( 'Footer Category Menu' ),
        'footer2'  => __( 'Footer Menu' ),
    ) );
}

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

}

add_action('acf/init', 'my_acf_blocks_init');
function my_acf_blocks_init() {

    // Check function exists.
    if( function_exists('acf_register_block_type') ) {

        // Register a posts block.
        acf_register_block_type(array(
            'name'              => 'posts',
            'title'             => __('Posts By Category'),
            'description'       => __('Posts by category block.'),
            'render_template'   => 'template/blocks/posts/posts.php',
            'category'          => 'common',
            'mode'              => 'edit'
        ));
        // Register a post block.
        acf_register_block_type(array(
            'name'              => 'post',
            'title'             => __('Sticky Posts'),
            'description'       => __('Display all sticky posts.'),
            'render_template'   => 'template/sticky-posts/sticky-posts.php',
            'category'          => 'common',
            'mode'              => 'edit'
        ));
        // Register a mailchimp block.
        acf_register_block_type(array(
            'name'              => 'mailchimp',
            'title'             => __('MailChimp'),
            'description'       => __('MailChimp Form'),
            'render_template'   => 'template/blocks/mailchimp/mailchimp.php',
            'category'          => 'common',
            'mode'              => 'edit'
        ));

        // Register a categories block.
        acf_register_block_type(array(
            'name'              => 'categories',
            'title'             => __('Categories Grid'),
            'description'       => __('Display selected categories as grid layout'),
            'render_template'   => 'template/blocks/categories/categories.php',
            'category'          => 'common',
            'mode'              => 'edit'
        ));

        // Register a Callout block.
        acf_register_block_type(array(
            'name'              => 'callout',
            'title'             => __('Callout'),
            'description'       => __('Display callout layout'),
            'render_template'   => 'template/blocks/callout/callout.php',
            'category'          => 'common',
            'mode'              => 'edit'
        ));

        // Register a comparison block.
        acf_register_block_type(array(
            'name'              => 'comparison',
            'title'             => __('Comparison'),
            'description'       => __('Display comparison layout'),
            'render_template'   => 'template/blocks/comparison/comparison.php',
            'category'          => 'common',
            'mode'              => 'edit'
        ));
    }
}

add_action('wp_enqueue_scripts', 'n2t_print_scripts');
function n2t_print_scripts(){
    wp_enqueue_script('jquery');
    if(is_front_page()){
        //wp_enqueue_script( 'jquery-ui-autocomplete' );
        //wp_enqueue_style('jquery-ui', '//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css');
    }
    wp_enqueue_script('main', get_template_directory_uri() . '/assets/script2.js', array('jquery'));
    wp_localize_script('main', 'n2t', ['ajaxURL' => admin_url('admin-ajax.php')]);
    wp_enqueue_style('main', get_stylesheet_uri());
}