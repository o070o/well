<?php get_header(); ?>
<div id="search-layout_1-0" class="comp search-layout">
	<section id="page-not-found-hero_1-0" class="comp page-not-found-hero hero" data-tracking-container="true">
		<div class="hero-container">
			<div class="g g-two-up">
				<div class="g-main">
					<h1 class="hero-title">
						404 - Ops đi lạc rồi!
					</h1>
					<div id="404-content_1-0" class="comp 404-content">
						<h2>Xin lỗi bạn địa chỉ này không tồn tại nội dung.</h2>
						<p>Tại Well.vn có rất nhiều nội dung hữu ích khác, hãy khám phá thêm nhé!</p>
						<a href="<?php echo home_url()?>" class="btn-link" aria-label="HOME">
							<button class="btn btn-divider" id="divider-button" style="display: inline;">
								<div class="btn-divider-inner">
									<span>TRANG CHỦ</span>
								</div>
							</button>
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<?php get_footer(); ?>
