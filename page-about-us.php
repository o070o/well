<?php
// =============================================================================
// TEMPLATE NAME: About Us
// -----------------------------------------------------------------------------
// A blank page for creating unique layouts.
//
// =============================================================================
get_header();the_post(); ?>
    <section id="hero_1-0" class="comp static-hero hero" data-tracking-container="true">
        <div class="hero-container">
            <div class="loc left">
                <img id="leftImage_1-0" class="comp leftImage leftImage image" src="https://www.verywellfit.com/static/4.177.0/image/about-us-left.png" alt="Running" />
            </div>
            <div class="g g-two-up">
                <div class="g-main">
                    <h1 class="hero-title">
                        <?php the_title(); ?>
                    </h1>
                    <p class="subheading"><?php the_field('sub_title'); ?></p>
                </div>
            </div>
            <div class="loc right">
                <img id="rightImage_1-0" class="comp rightImage rightImage image" src="https://www.verywellfit.com/static/4.177.0/image/about-us-right.png" alt="Nutrition facts" />
            </div>
    </div>
    </section>
	<article class="comp right-rail static-page-content article">
		<div id="toc_1-0" class="comp right-rail__offset lock-journey 1 toc static-toc closed" data-tracking-container="true" style="">
			<div class="toc-wrapper">
				<strong class="toc-heading" data-click-tracked="true">
					Table of Contents
					<svg class="icon icon-arrow-down ">
						<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-arrow-down"></use>
					</svg>
				</strong>
				<div class="toc-scroll expert-content">
					<div id="mntl-toc_1-0" class="comp toc-list 1 mntl-toc mntl-block" columnnum="1" data-offset="10">
						<div id="mntl-toc__inner_1-0" class="comp mntl-toc__inner mntl-block">
							<div id="mntl-toc__heading_1-0" class="comp mntl-toc__heading mntl-block">
								<span id="mntl-toc__heading-text_1-0" class="comp mntl-toc__heading-text mntl-text-block">
								Table of Contents</span>
								<div id="mntl-toc-toggle_1-0" class="comp mntl-toc-toggle mntl-block">
									<button id="mntl-toc-toggle__btn_1-0" class="comp js-mntl-toc-toggle 1 mntl-toc-toggle__btn mntl-text-block" data-collapsed-text="Expand" data-offset="10" columnnum="1" data-expanded-text="Collapse">
									Expand</button>
								</div>
							</div>
							<ul class="comp mntl-toc__list mntl-block">
								<?php $i = 0; while( have_rows('table_of_content') ) : the_row(); ?>
								<li class="comp 1 mntl-toc__list-item mntl-block">
									<div class="comp mntl-toc__list-item-heading mntl-block">
										<a href="#<?php echo sanitize_title(get_sub_field('heading')); ?>" class="mntl-toc__list-item-link mntl-text-link mntl-toc__list-item-link js-mntl-toc-link <?php echo $i==0 ? 'active': ''; ?>">
											<span class="link__wrapper"><?php the_sub_field('heading'); ?></span>
										</a>
									</div>
								</li>
								<?php $i++; endwhile; ?>
							</ul>
						</div>
					</div>
					<div class="view-all-container">
						<button class="text-btn view-all">
							View All
							<svg class="text-btn-icon">
								<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-circle-arrow-down"></use>
							</svg>
						</button>
					</div>
					<button class="close-button">
						<svg class="icon icon-x">
							<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-x"></use>
						</svg>
					</button>
				</div>
			</div>
		</div>
		<div class="comp structured-content article-content expert-content right-rail__offset lock-journey mntl-sc-page mntl-block">
			<?php while( have_rows('table_of_content') ) : the_row(); ?>
			<a class="heading-toc" id="<?php echo sanitize_title(get_sub_field('heading')); ?>"></a>
			<?php the_sub_field('content'); ?>
			<?php endwhile; ?>
		</div>
	</article>
<?php get_footer(); ?>