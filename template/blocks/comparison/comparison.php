<?php

/**
 * Comparison Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'n2t-block-comparison-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'n2t-block-comparison';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

?>
<div id="mntl-sc-block_<?php echo $block['id']; ?>>" class="comp mntl-sc-block mntl-sc-block-comparisonlist <?php echo $className; ?>">
    <div class="mntl-sc-block-comparisonlist__wrapper">
        <span class="mntl-sc-block-comparisonlist__heading"><?php the_field('heading_left') ?></span>
        <ul class="mntl-sc-block-comparisonlist__list">
            <?php the_field('list_left') ?>
        </ul>
    </div>
    <div class="mntl-sc-block-comparisonlist__wrapper">
        <span class="mntl-sc-block-comparisonlist__heading"><?php the_field('heading_right') ?></span>
        <ul class="mntl-sc-block-comparisonlist__list">
            <?php the_field('list_right') ?>
        </ul>
    </div>
</div>