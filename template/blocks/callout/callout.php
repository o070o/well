<?php

/**
 * Posts Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'n2t-block-callout-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'n2t-block-callout';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

$style = get_field('style');
?>

<div id="mntl-sc-block_<?php echo $block['id']; ?>" class="comp theme-<?php echo $style; ?> mntl-sc-block mntl-sc-block-callout mntl-block <?php echo $className; ?>">
    <h3 class="comp mntl-sc-block-callout-heading mntl-text-block">
        <?php the_field('callout_heading'); ?>
    </h3>
    <div class="comp expert-content mntl-sc-block-callout-body mntl-text-block">
        <?php the_field('callout_content'); ?>
    </div>
    <?php if($style == 'exclamation'): ?>
    <svg class="icon icon-exclamation ">
        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-exclamation"></use>
    </svg>
    <?php endif; ?>
    <?php if($style == 'pullquote'): ?>
    <div class="comp theme-pullquote__author mntl-text-block">
        <?php the_field('callout_heading'); ?>
    </div>
    <?php endif; ?>
</div>