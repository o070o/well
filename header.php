<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
	<head class="loc head">
        <meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://fonts.googleapis.com/css2?family=Noto+Serif:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">
		<link type="text/css" rel="stylesheet" data-glb-css="" href="<?php echo get_template_directory_uri(); ?>/assets/style.css">
        <?php if(!is_front_page()): ?>
        <link type="text/css" rel="stylesheet" data-glb-css="" href="<?php echo get_template_directory_uri(); ?>/assets/single.css">
        <?php endif; ?>
        <?php wp_head(); ?>
		<script type="text/javascript">
			(function(c,l,a,r,i,t,y){
				c[a]=c[a]||function(){(c[a].q=c[a].q||[]).push(arguments)};
				t=l.createElement(r);t.async=1;t.src="https://www.clarity.ms/tag/"+i;
				y=l.getElementsByTagName(r)[0];y.parentNode.insertBefore(t,y);
			})(window, document, "clarity", "script", "40do7lkfdp");
		</script>
	</head>
	<body <?php body_class(); ?>>
        <?php get_template_part('template/svg'); ?>
		<header class="comp header" role="banner" data-tracking-container="true">
			<div class="menu-button-container">
				<button class="menu-button" aria-label="Menu">
					<div class="menu-button-inner">
						<svg class="icon icon-hamburger">
							<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-hamburger"></use>
						</svg>
						<svg class="icon icon-x">
							<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-x"></use>
						</svg>
						<span>Menu</span>
						<div></div>
					</div>
				</button>
			</div>
			<a class="comp header-logo logo verywellfit" rel="home" href="<?php echo home_url('/') ?>">
				<span class="is-vishidden">Well.vn</span>
				<svg height="50" viewBox="0 0 895 180">
					<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#verywell-logo" class="verywell-logo-use"></use>
					<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#verywellfit" class="verywellfit-use"></use>
					<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#verywellfit-footer" class="verywellfit-use-footer"></use>
				</svg>
			</a>
			<div class="header-title"><?php echo get_bloginfo ( 'description' ); ?></div>
            <?php if(is_front_page()): ?>
			<div class="loc header-secondary">
				<button id="header-search-button_1-0" class="comp header-search-button search-button jetpack-instant-search__open-overlay-button">
					<span class="is-vishidden">Tìm kiếm</span>
					<svg class="icon icon-magnifying-glass">
						<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-magnifying-glass"></use>
					</svg>
				</button>
			</div>
            <?php else: ?>
            <div class="nav">
                <ul class="nav-list">
                    <?php
                        wp_nav_menu( array(
                            'items_wrap' => '%3$s',
                            'theme_location' => 'primary',
                            'container'=>'',
                            'before'    => '<span class="nav-title">',
                            'after'     => '</span>',
                            'walker' => new Primary_Walker_Nav_Menu()
                        ) );
                    ?>
                </ul>
                <div class="loc header-secondary">
                    <button id="header-search-button_1-0" class="comp header-search-button search-button jetpack-instant-search__open-overlay-button">
                        <span class="is-vishidden">Tìm kiếm</span>
                        <svg class="icon icon-magnifying-glass">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-magnifying-glass"></use>
                        </svg>
                    </button>
                </div>
            </div>
            <?php endif; ?>
			<div class="header-nav-panel">
				<nav id="fullscreen-nav_1-0" class="comp fullscreen-nav fullscreen-nav-alternate" role="navigation" data-tracking-container="true">
					<div id="general-search_1-0" class="comp general-search" data-tracking-container="true">
						<form class="general-search-form" role="search" action="<?php echo home_url('/') ?>" method="get" data-suggestion="verywell">
							<div class="input-group">
								<button class="btn btn-submit">
									<span class="is-vishidden">Tìm kiếm</span>
									<svg class="icon icon-magnifying-glass">
										<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-magnifying-glass"></use>
									</svg>
								</button>
								<button class="btn btn-clear is-hidden">
									<span class="is-vishidden">Làm sạch</span>
									<svg class="icon icon-x">
										<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-x"></use>
									</svg>
								</button>
								<input type="text" name="s" id="search-input" class="general-search-input header-search-input" placeholder="Bạn đang tìm kiếm điều gì?" aria-label="Bạn đang tìm kiếm điều gì?" required="required" value="" autocomplete="off">
								<button class="btn btn-bright btn-go">Tìm kiếm</button>
							</div>
						</form>
					</div>
					<div class="breadcrumbs-tags-section">
                        <div class="comp breadcrumbs-tools" data-tracking-container="true">
                            <span class="breadcrumbs-list-subtitle">Nội dung đề xuất</span>
                            <ul class="breadcrumbs-list-list">
                                <?php
                                    wp_nav_menu( array(
                                        'items_wrap' => '%3$s',
                                        'theme_location' => 'tools',
                                        'container'=>'',
                                        'link_class'   => 'breadcrumbs-list-link'
                                    ) );
                                ?>
                            </ul>
                        </div>
						<section id="tags-section_1-0" class="comp tags-section">
							<ul id="tags-nav_1-0" class="comp tags-nav link-list mntl-block">
								<?php
                                    $bookmarks = get_bookmarks( array(
                                        'orderby'        => 'name',
                                        'order'          => 'ASC',
                                        'category_name'  => 'Trending Topics'
                                    ) );
                                    if(!empty($bookmarks)){
                                    foreach ( $bookmarks as $bookmark ) {
                                ?>
                                <li id="link-list-items_<?php echo $bookmark->link_id; ?>" class="comp tags-nav-item link-list-items link-list-item" data-ordinal="1">
                                    <a href="<?php echo $bookmark->link_url; ?>" class="link-list-link tags-nav-link"> <?php echo $bookmark->link_name; ?></a>
                                </li>
                                <?php }} ?>
							</ul>
						</section>
					</div>
					<ul class="fullscreen-nav-list">
                        <?php
                            wp_nav_menu( array(
                                'items_wrap' => '%3$s',
                                'theme_location' => 'header_fullscreen',
                                'container'=>'',
                                'before'    => '<span class="fullscreen-nav-title">',
                                'after'     => '</span>',
                                'walker' => new FullScreen_Walker_Nav_Menu()
                            ) );
                        ?>
					</ul>
					<div class="loc bottom fullscreen-nav-bottom">
						<div id="fullscreen-nav-bottom-left_1-0" class="comp fullscreen-nav-bottom-left mntl-block">
							<ul id="trusted-links_1-0" class="comp trusted-links link-list mntl-block" data-tracking-container="true">
								<?php
                                    wp_nav_menu( array(
                                        'items_wrap' => '%3$s',
                                        'theme_location' => 'header',
                                        'container'=>''
                                    ) );
                                ?>
							</ul>
						</div>
						<div id="fullscreen-nav-bottom-right_1-0" class="comp fullscreen-nav-bottom-right mntl-block">
							<div id="copyright_1-0" class="comp copyright mntl-text-block">
								© 2020 Well.vn - Lan tỏa giá trị sống xanh & tích cực 
							</div>
						</div>
					</div>
				</nav>
			</div>
		</header>
		<?php if(is_single()): ?>
		<div class="comp has-left-label has-right-label leaderboard--full-bleed leaderboard--header mntl-leaderboard-header mntl-flexible-leaderboard mntl-flexible-ad mntl-gpt-adunit gpt leaderboard not-sticky" data-ad-width="728" data-ad-height="90">
			<div id="leaderboard" class="wrapper" data-google-query-id="1234567890" style="height: 90px"></div>
		</div>
		<div class="comp js-rollaway-spacer mntl-leaderboard-spacer mntl-block"></div>
		<?php endif; ?>
		<main id="main" class="loc main" role="main">