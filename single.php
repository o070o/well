<?php
    global $post;
    get_header();
    the_post();
    $top_category_id_3 = n2t_get_top_category(null, 3);
    $top_category_3 = get_category($top_category_id_3);

    $top_category_id_2 = n2t_get_top_category(null, 2);
    $top_category_2 = get_category($top_category_id_2);

    $top_category_id_1 = n2t_get_top_category(null, 1);
    $top_category_1 = get_category($top_category_id_1);

    $current_cat = wp_get_post_categories( $post->ID )[0];

    $level1 = $level2 = false;
?>
    <article class="comp right-rail article">
        <header class="loc article-header article-header">
            <div class="comp article-preheading mntl-block">
                <span class="comp news-badge-wrapper mntl-block"></span>
                <div class="comp taxlevel-2 breadcrumbs">
                    <?php if($top_category_id_3): ?>
                    <div class="breadcrumb-container">
                        <a href="<?php echo get_category_link($top_category_id_3); ?>" class="breadcrumb-2 breadcrumbs-link"><?php echo $top_category_3->name; ?></a>
                        <svg class="icon icon-arrow-right 2"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-arrow-right"></use></svg>
                    </div>
                    <?php endif; ?>
                    <?php if($top_category_id_2): $level2 = true; ?>
                    <div class="breadcrumb-container">
                        <a href="<?php echo get_category_link($top_category_id_2); ?>" class="breadcrumb-2 breadcrumbs-link"><?php echo $top_category_2->name; ?></a>
                        <svg class="icon icon-arrow-right 2"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-arrow-right"></use></svg>
                    </div>
                    <?php endif; ?>
                    <?php if($top_category_id_1): $level1 = true; ?>
                    <div class="breadcrumb-container">
                        <a href="<?php echo get_category_link($top_category_id_1); ?>" class="breadcrumb-2 breadcrumbs-link"><?php echo $top_category_1->name; ?></a>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
            <h1 class="comp article-heading">
                <?php the_title(); ?>
            </h1>
            <h2 id="article-subheading_1-0" class="comp article-subheading"><?php the_field('sub_title'); ?></h2>
            <div class="comp not-news article-meta mntl-block">
                <div class="comp article-meta__wrapper mntl-block">
                    <div class="comp article-byline mntl-byline mntl-block">
                        <span class="comp mntl-byline__text mntl-text-block">Đăng bởi:</span>
                        <div class="comp mntl-byline__name mntl-block mntl-dynamic-tooltip--trigger" data-tooltip="<p>placeholder</p>" data-tooltip-position-x="left" data-tooltip-position-y="bottom">
                            <a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" class=" mntl-byline__link mntl-text-link">
                                <span class="link__wrapper"><?php the_author(); ?></span>
                            </a>
                            <!-- Author Tooltip -->
                            <div id="mntl-dynamic-tooltip" class="mntl-dynamic-tooltip" data-tracking-container="true">
                                <div class="comp mntl-bio-tooltip bio-tooltip mntl-block mntl-dynamic-tooltip--content">
                                    <div class="comp mntl-bio-tooltip__top mntl-block">
                                        <div class="comp mntl-bio-tooltip__image-wrapper mntl-block" data-defer="load" data-defer-params="">
                                            <img src="<?php the_field('author_image', 'user_' . get_the_author_meta( 'ID' )); ?>" alt="<?php the_author(); ?>" class="mntl-bio-tooltip__image mntl-image" />
                                        </div>
                                        <div class="comp mntl-bio-tooltip__info mntl-block">
                                            <div id="mntl-bio-tooltip__name_1-0" class="comp mntl-bio-tooltip__name mntl-block">
                                                <span id="mntl-bio-tooltip__prelink_1-0" class="comp mntl-bio-tooltip__prelink mntl-text-block"></span>
                                                <a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" id="mntl-bio-tooltip__link_1-0" class="mntl-bio-tooltip__link mntl-text-link" data-tracking-container="true">
                                                    <span class="link__wrapper"><?php the_author(); ?></span>
                                                </a>
                                                <span id="mntl-bio-tooltip__postlink_1-0" class="comp mntl-bio-tooltip__postlink mntl-text-block"></span>
                                            </div>
                                            <div class="comp bio-social-follow__list bio-social-follow social-follow mntl-block" data-tracking-container="true">
                                                <div id="social-follow__title_2-0" class="comp social-follow__title mntl-text-block"></div>
                                                <div id="social-follow__intro_2-0" class="comp social-follow__intro mntl-text-block"></div>
                                                <ul id="social-follow__list_2-0" class="comp social-follow__list mntl-block">
                                                    <?php
                                                        $socials = get_field('socials', 'user_' . get_the_author_meta( 'ID' ));
                                                        if(!empty($socials)):
                                                            foreach ($socials as $name => $url):
                                                    ?>
                                                    <li class="comp bio-social-follow__btn social-follow__item social-follow__btn mntl-block">
                                                        <a href="<?php echo $url; ?>" target="_blank" rel="noopener" class="  social-follow__link mntl-text-link social-follow__link--<?php echo $name; ?>">
                                                            <span class="comp is-vishidden mntl-text-block"><?php echo $name; ?></span>
                                                            <svg class="icon icon-<?php echo $name; ?> ">
                                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-<?php echo $name; ?>"></use>
                                                            </svg>
                                                        </a>
                                                    </li>
                                                    <?php endforeach; endif; ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="mntl-bio-tooltip__bottom_1-0" class="comp mntl-bio-tooltip__bottom mntl-block">
                                        <div id="mntl-bio-tooltip__bio_1-0" class="comp mntl-bio-tooltip__bio mntl-text-block">
                                            <p><?php the_field('short_description', 'user_' . get_the_author_meta( 'ID' )); ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>                            
                            <!-- End / Author Tooltip -->
                        </div>
                    </div>
                    <span class="comp byline-article-updated mntl-text-block">
                        Cập nhật lúc: <?php the_date('F d, Y') ?>
                    </span>
                </div>
            </div>
        </header><!-- Header Post Meta  -->
        <?php if(has_post_thumbnail()): ?>
        <figure id="figure-article_1-0" class="comp figure-landscape right-rail__offset lock-journey figure-article mntl-block" data-tracking-container="true">
            <div id="figure-article__media_1-0" class="comp figure-article__media mntl-block">
                <div class="img-placeholder" style="padding-bottom:69.2%;">
                    <?php echo get_the_post_thumbnail(get_the_ID(), 'full', ['class'=>'figure-article__image mntl-primary-image']); ?>
                </div>
            </div>
            <?php
                $get_description = get_post(get_post_thumbnail_id())->post_excerpt;
                if(!empty($get_description)):
            ?>
            <figcaption id="figure-article__caption_1-0" class="comp figure-article__caption mntl-figure-caption figure-article-caption">
                <span class="figure-article-caption-owner">
                    <p><?php echo $get_description; ?></p>
                </span>
            </figcaption>
            <?php endif; ?>
        </figure><!-- Post Featured Image -->
        <?php endif; ?>
        <div class="comp breadcrumbs-list">
            <?php
                if($top_category_id_3){
                    $parent_id = $top_category_id_3;
                } else if($top_category_id_2){
                    $parent_id = $top_category_id_2;
                }
                $parent_cat = get_category( $parent_id );
            ?>
            <div class="breadcrumbs-list-header">
                <a href="<?php echo get_category_link($parent_cat); ?>" class="breadcrumbs-list-image">
                    <img src="<?php echo get_field('background_image', $parent_cat); ?>" alt="<?php echo $parent_cat->name;  ?>">
                </a>
                <span class="breadcrumbs-list-subtitle">
                    <a href="<?php echo get_category_link($parent_cat); ?>">
                        <?php echo $parent_cat->name;  ?>
                        <svg class="breadcrumbs-list-icon icon-empty-caret">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-empty-caret"></use>
                        </svg>
                    </a>
                </span>
            </div>
            <ul class="breadcrumbs-list-list">
                <?php
                    $childs = get_categories([
                        'parent' => $parent_id,
                        'hide_empty' => 0
                    ]);
                    if(!empty($childs)){
                        foreach ($childs as $child) {
                ?>
                <li class="breadcrumbs-list-item <?php echo $child->term_id == $current_cat->term_id ? 'is-active' : ''; ?>">
                    <a href="<?php echo get_category_link( $child ) ?>" data-ordinal="1" class="breadcrumbs-list-link <?php echo $child->term_id == $current_cat->term_id ? 'is-active' : ''; ?>">
                        <?php echo $child->name; ?>
                        <svg class="breadcrumbs-list-icon icon-empty-caret">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-empty-caret"></use>
                        </svg>
                    </a>
                </li>
                <?php }} ?>
            </ul>
        </div><!-- Breadcrumb Post -->
        <div id="list-sc_1-0" class="comp article-content expert-content list lock-journey no-commerce list-sc mntl-block">
            <?php the_content(); ?>
            <div id="cal1">&nbsp;</div>
            <div id="cal2">&nbsp;</div>
            <div id="tooltip">
                <a class="close-report-form" href="#">x</a>
                <?php 
                    echo apply_filters( 'the_content', '<!-- wp:jetpack/contact-form {"subject":"New feedback received from your website"} -->

                    <!-- wp:jetpack/field-textarea {"label":"Đoạn text mà bạn muốn báo lỗi"} /-->

                    <!-- wp:jetpack/field-textarea {"label":"Đoạn text sau khi chỉnh sửa"} /-->

                    <!-- wp:jetpack/button {"element":"button","text":"Gửi"} /-->
                    <!-- /wp:jetpack/contact-form -->');
                ?>
            </div>
        </div><!-- Post Content -->
        <div class="comp post-article mntl-block">
            <div class="comp article-feedback mntl-article-feedback">
                <div class="article-feedback__rating-section js-rating-section">
                    <div class="article-feedback__heading">Nội dung này có hữu ích với bạn?</div>
                    <button id="thumbs-up-button_1-0" class="comp article-feedback__rating-button js-rating-button thumbs-up-button mntl-button" aria-label="Thumbs Up" data-thumbs-signal="THUMBS_UP">
                        <svg class="icon icon-thumbsup mntl-button__icon article-feedback__rating-icon">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-thumbsup"></use>
                        </svg>
                    </button>
                    <button id="thumbs-down-button_1-0" class="comp article-feedback__rating-button js-rating-button thumbs-down-button mntl-button" aria-label="Thumbs Down">
                        <svg class="icon icon-thumbsdown mntl-button__icon article-feedback__rating-icon">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-thumbsdown"></use>
                        </svg>
                    </button>
                </div>
                <div class="article-feedback__success-section js-success-section is-hidden">
                    <div class="article-feedback__heading">Cảm ơn bạn đã nhận xét!</div>
                    <div class="loc success-section">
                        <div id="newsletter-signup-vue_39-0" class="comp newsletter-signup-vue nl-loaded">
                            <script>(function() {
                                window.mc4wp = window.mc4wp || {
                                    listeners: [],
                                    forms: {
                                        on: function(evt, cb) {
                                            window.mc4wp.listeners.push(
                                                {
                                                    event   : evt,
                                                    callback: cb
                                                }
                                            );
                                        }
                                    }
                                }
                            })();
                            </script>
                            <form class="newsletter-signup-vue__form" id="mc4wp-form-2" method="post" data-id="44" data-name="Newsletter">
                                <span class="newsletter-signup-vue__header"></span>
                                <p class="newsletter-signup-vue__subheader">Đăng ký nhận các lời khuyên hữu ích về tập luyện sức khỏe.</p>
                                <div class="newsletter-signup-vue__wrapper">
                                    <div class="newsletter-signup-vue__input-wrapper input-group">
                                        <input type="email" name="email" placeholder="Vui lòng nhập email" required="required" aria-label="Vui lòng nhập email" class="mntl-newsletter-submit__input mntl-newsletter-submit__button newsletter-signup-vue__input"> <button type="submit" class="btn newsletter-signup-vue__button">Đăng ký</button>
                                    </div>
                                    <span class="newsletter-signup-vue__header newsletter-signup-vue__header--success" style="display: none;">Đăng ký thành công!</span>
                                    <p class="success-message" style="display: none;">Cảm ơn bạn đã đăng ký nhận tin.</p>
                                    <p class="error-message" style="display: none;">Vui lòng thực hiện lại.</p>
                                </div>
                                <label style="display: none !important;">Leave this field empty if you're human: <input type="text" name="_mc4wp_honeypot" value="" tabindex="-1" autocomplete="off"></label>
                                <input type="hidden" name="_mc4wp_timestamp" value="1602777328">
                                <input type="hidden" name="_mc4wp_form_id" value="44">
                                <input type="hidden" name="_mc4wp_form_element_id" value="mc4wp-form-2">
                                <div class="mc4wp-response"></div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="article-feedback__feedback-section js-feedback-section is-hidden">
                    <div class="article-feedback__heading">Điều gì bạn chưa hài lòng?</div>
                    <?php 
                        echo apply_filters( 'the_content', '<!-- wp:jetpack/contact-form {"subject":"New feedback received from your website"} -->
                        <!-- wp:jetpack/field-radio {"label":"Was this page helpful?","options":["Yes","No"]} /-->

                        <!-- wp:jetpack/field-radio {"label":"What are your concerns?","options":["Inaccurate","Hard to Understand","Other"]} /-->

                        <!-- wp:jetpack/field-textarea {"label":"How could we improve?"} /-->

                        <!-- wp:jetpack/button {"element":"button","text":"Send Feedback"} /-->
                        <!-- /wp:jetpack/contact-form -->');
                    ?>
                </div>
            </div>
            <?php //if(!empty(the_field('source'))): ?>
            <div class="comp sources mntl-block">
                <div class="comp mntl-article-sources mntl-expandable-block" data-scroll-offset="70">
                    <div class="loc toggle-content">
                        <div class="comp mntl-article-sources__wrapper mntl-block" data-click-tracked="true">
                            <span id="mntl-article-sources__heading_1-0" class="comp mntl-article-sources__heading mntl-text-block">Nguồn bài viết</span>
                            <svg class="icon show-icon ">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#show-icon"></use>
                            </svg>
                            <svg class="icon hide-icon ">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#hide-icon"></use>
                            </svg>
                        </div>
                    </div>
                    <div class="loc expandable-content">
                        <div class="comp source-guidelines mntl-text-block">
                            <?php the_field('source_text', 'option'); ?>
                        </div>
                        <div id="mntl-article-sources__citation-sources_1-0" class="comp mntl-article-sources__citation-sources mntl-citation-sources mntl-sources">
                            <?php the_field('source'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php //endif; ?>
        </div>
    </article>
    <?php
        $related = get_posts(
            array(
                'category__in' => wp_get_post_categories( $post->ID ),
                'numberposts'  => 8,
                'post__not_in' => array( $post->ID )
            )
        );
        if( $related ):
    ?>
    <div id="prefooter_1-0" class="comp prefooter mntl-block">
        <div id="prefooter-content_1-0" class="comp prefooter-content mntl-block">
            <section id="related-article-list_2-0" class="comp related-article-list article-list">
                <span class="section-title">Gợi ý bạn đọc thêm</span>
                <div class="loc content section-body">
                    <ul id="block-list_2-0" class="comp g g-four-up block-list" data-chunk="">
                        <?php
                            foreach( $related as $post ) {
                                setup_postdata($post);
                                get_template_part('template/loop/content');
                            }
                            wp_reset_postdata();
                        ?>
                    </ul>
                </div>
            </section>
        </div>
    </div>
    <!-- Related Posts -->
    <?php endif; ?>
<?php get_footer(); ?>
